from django.shortcuts import render, get_object_or_404, redirect
from projects.models import Project

# from tasks.forms import TaskForm
from projects.forms import ProjectForm
from django.contrib.auth.decorators import login_required

# Create your views here.


@login_required
def list_projects(request):
    projects = Project.objects.filter(owner=request.user)
    context = {
        "list_project": projects,
    }
    return render(request, "projects/list.html", context)


@login_required
def show_projects(request, id):
    project = get_object_or_404(Project, id=id)
    context = {"detail_project": project}
    return render(request, "projects/details.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            project = form.save(False)
            project.owner = request.user
            project.save()
            return redirect("list_projects")
    else:
        form = ProjectForm()

        context = {
            "form": form,
        }
    return render(request, "projects/create.html", context)


# @login_required
# def create_tasks(request):
#     if request.method == "POST":

#         form = TaskForm(request.POST)
#         if form.is_valid():
#             task = form.save()
#             task.assignee = request.user
#             task.save()
#             return redirect("list_projects")
#     else:
#         form = TaskForm()

#         context = {
#             "form": form,
#         }
#     return render(request, "projects/tasks.html", context)


# def show_my_tasks(request):
#     tasks = Task.objects.filter(assignee=request.user)
#     context = {
#         "show_my_tasks": tasks,
#     }
#     return render(request, "projects/list.html", context)


# def list_of_projects(request):
#     project = Project.objects.filter(owner=request.user)
#     context = {
#         "list_of_project": project,
#     }
#     return render(request, "projects/list.html", context)

# @login_required
# def project_details(request, id):
#     project_detail = Project.objects.get(id=id)
#     context = {
#         "project_detail_object": project_detail
#     }
#     return render(request, "tasks/detail.html", context)
